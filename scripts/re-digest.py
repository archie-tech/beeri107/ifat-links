import os
import hashlib
import json


def calculate_file_digest(file):
    with open(file, "rb") as f:
        file_hash = hashlib.md5()
        while chunk := f.read(8192):
            file_hash.update(chunk)
    return file_hash.hexdigest()


if __name__ == "__main__":
    for doc_file in os.listdir("local/docs"):
        with open(f"local/docs/{doc_file}") as f:
            doc = json.load(f)
            if doc['dcFormat'] == "pdf":
                file_name = doc_file.replace(".json", "")
                if os.path.isfile(f"local/files/{file_name}"):
                    file_digest = calculate_file_digest(f"local/files/{file_name}")
                    if file_digest != doc['fileDigest']:
                        doc['fileDigest'] = file_digest
                        with open(f'local/docs.reindex/{doc_file}', 'w') as f:
                            json.dump(doc, f, indent=4)
