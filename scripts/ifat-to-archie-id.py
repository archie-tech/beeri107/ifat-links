import os
import json

DOCS_FOLDER = "local/docs.done"

for doc_file in os.listdir(DOCS_FOLDER):
    file_name = doc_file.replace(".json", "")
    with open(f"{DOCS_FOLDER}/{doc_file}") as f:
        doc = json.load(f)
    key = f'{doc["id"]}.{doc["dcFormat"]}'
    print(f"{file_name},{key}")
