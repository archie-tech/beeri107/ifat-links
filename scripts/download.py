#!/usr/bin/env python3

import os
import argparse
import csv
import subprocess
import requests
import logging.config
import yaml
from my_html_parser import MyHTMLParser

LOGGER = logging.getLogger("com.mycompnay.app")


def get_existing_items(existing_items_file):
    ids = set(())
    with open(existing_items_file, newline='') as f:
        lines = f.readlines()
        for line in lines:
            filename = line.split(',')[0]
            id = filename.split('.')[0]
            ids.add(id)
    LOGGER.info(f"{len(ids)} existing items found")
    return ids


def download_single_file(url, outfile):
    cmd = ["curl", "--silent", "--show-error", "--output", outfile, url]
    if not os.path.exists(outfile):
        LOGGER.info(cmd)
        try:
            subprocess.check_call(cmd)
        except subprocess.CalledProcessError as e:
            LOGGER.warning(e)


def main_task(in_file, out_folder, existing_items_file):
    user_agent = {'User-agent': 'Mozilla/5.0'}
    parser = MyHTMLParser()
    existing_items = get_existing_items(existing_items_file)
    LOGGER.info(f"processing links from file {in_file}")
    with open(in_file, newline='') as f:
        # ItemID,Media,Link. Media = אינטרנט | טלויזיה | עיתונות | רדיו
        reader = csv.DictReader(f)
        for record in reader:
            if record["ItemID"] not in existing_items:
                match record["אמצעי מדיה"]:
                    case "טלויזיה":
                        page = requests.get(record["לינק"], allow_redirects=True, headers=user_agent, timeout=10).text
                        parser.set_ext("mp4")
                        parser.feed(page)
                        download_single_file(url=parser.url, outfile="{}/{}.mp4".format(out_folder, record["ItemID"]))
                    case "רדיו":
                        page = requests.get(record["לינק"], allow_redirects=True, headers=user_agent, timeout=10).text
                        parser.set_ext("mp3")
                        parser.feed(page)
                        download_single_file(url=parser.url, outfile="{}/{}.mp3".format(out_folder, record["ItemID"]))
                    case "עיתונות":
                        url = requests.get(record["לינק"], allow_redirects=True, headers=user_agent, timeout=10).url
                        if url.lower().endswith('pdf'):
                            download_single_file(url=url, outfile="{}/{}.pdf".format(out_folder, record["ItemID"]))
                        else:
                            LOGGER.warning("invalid file name in url {}".format(url))
                    case "אינטרנט":
                        LOGGER.info(f'skipping download_complete_page for item {record["ItemID"]}')
                    case _:
                        LOGGER.warning("invalid media {} in item {}".format(record["אמצעי מדיה"], record["ItemID"]))
                # time.sleep(os.getenv("SLEEP", 1))
    LOGGER.info("the operation completed successfully")


def parse_arguments():
    epilog = "Example: app.py -i ifat.csv -o local -e existing_items.csv"
    parser = argparse.ArgumentParser(description='This script downloads files from URLs.', epilog=epilog)
    parser.add_argument('--in_file', '-i', type=str, required=True, help='CSV input file from ifat service')
    parser.add_argument('--out_folder', '-o', type=str, required=True, help='output folder')
    parser.add_argument('--existing_items', '-e', type=str, required=True, help='map of ifat id to archie id')
    args = parser.parse_args()
    return args


def config_logging():
    with open("logging.yml") as f:
        conf = yaml.safe_load(f)
    conf["loggers"]["com.mycompnay.app"]["level"] = os.getenv("LOGGING_LEVEL", "INFO")
    logging.config.dictConfig(conf)


if __name__ == "__main__":
    args = parse_arguments()
    config_logging()
    main_task(in_file=args.in_file, out_folder=args.out_folder, existing_items_file=args.existing_items)
