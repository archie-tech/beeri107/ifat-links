import os
import json
import mimetypes
import boto3


PROFILE = os.environ["AWS_PROFILE"]  # arinamal-dev | beeri
BUCKET = os.environ["BUCKET"]  # public-assets.archie.beeri.dev.arinamal.com | archie-beeri-public

print(f"using profile {PROFILE}")

S3_CLIENT = boto3.client('s3')


for doc_file in os.listdir("local/docs"):
    file_name = doc_file.replace(".json", "")
    with open(f"local/docs/{doc_file}") as f:
        doc = json.load(f)
    key = f'originals/{doc["id"]}.{doc["dcFormat"]}'
    extra_args = {
        "ContentDisposition": "attachment",
        "ContentType": mimetypes.guess_type(file_name)[0]
    }
    print(f"upload local/files/{file_name} to {BUCKET}/{key}")
    S3_CLIENT.upload_file(Bucket=BUCKET, Key=key, Filename=f"local/files/{file_name}", ExtraArgs=extra_args)
    os.rename(f"local/docs/{doc_file}", f"local/docs.done/{doc_file}")
