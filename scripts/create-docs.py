#!/usr/bin/env python3

import os
import argparse
import logging.config
import csv
import json
import uuid
import hashlib
from datetime import datetime
import yaml  # pip install PyYAML

LOGGER = logging.getLogger("com.mycompnay.app")


def calculate_file_digest(file):
    with open(file, "rb") as f:
        file_hash = hashlib.md5()
        while chunk := f.read(8192):
            file_hash.update(chunk)
    logging.debug("md5 digest of file {} is {}".format(file, file_hash.hexdigest()))
    return file_hash.hexdigest()


def get_dc_type(file_name_extention):
    if file_name_extention in ["pdf"]:
        return "text"
    if file_name_extention in ["jpg", "jpeg"]:
        return "image"
    if file_name_extention in ["mp3"]:
        return "audio"
    if file_name_extention in ["mp4"]:
        return "video"
    raise ValueError(f"unknown file name extention: {file_name_extention}")


def format_date(input_date_string):
    # Define the input and output formats
    input_format = "%d/%m/%y %H:%M"
    output_format = "%Y-%m-%dT%H:%M:%S"
    # Parse the input date string
    parsed_date = datetime.strptime(input_date_string, input_format)
    # Convert to the desired format
    formatted_date = parsed_date.strftime(output_format) + "Z"
    return formatted_date


def get_description(record):
    description = []
    for field in ["אמצעי מדיה", "News paper", "Reporters", "הערות"]:  # Appendage
        text = record[field].strip()
        if text != '':
            if text not in description:
                description.append(text)
    return " - ".join(description)


def add_meta_data(docs, index):
    LOGGER.info(f'adding properties to docs from index')
    for filename in docs:
        ifat_id = filename.split(".")[0]
        docs[filename]["id"] = str(uuid.uuid4())
        docs[filename]["dcAccessRights"] = "public"
        docs[filename]["dcIsPartOf"] = "עיתונות חוץ"
        docs[filename]["dcSubject"] = ["השבת השחורה", "7 באוקטובר", "חרבות ברזל"]
        docs[filename]["dcTitle"] = index[ifat_id]["Title"]
        docs[filename]["dcDate"] = format_date(index[ifat_id]["Publish Date"])
        docs[filename]["dcDescription"] = get_description(index[ifat_id])
        docs[filename]["dcFormat"] = filename.split(".")[1]
        docs[filename]["dcType"] = get_dc_type(docs[filename]["dcFormat"])
    return docs


def get_files(in_folder):
    LOGGER.info(f'processing files from {in_folder}')
    docs = {}
    for filename in os.listdir(in_folder):
        docs[filename] = {"fileDigest": calculate_file_digest(f"{in_folder}/{filename}")}
    return docs


def get_index(index_file):
    LOGGER.info(f'processing index file {index_file}')
    index = {}
    with open(index_file, newline='') as infile:
        reader = csv.DictReader(infile)
        for record in reader:
            index[record["ItemID"]] = record
    return index


def save_docs(docs, out_folder):
    LOGGER.info(f'saving {len(docs)} docs to {out_folder}')
    for filename in docs:
        with open(f'{out_folder}/{filename}.json', 'w') as f:
            json.dump(docs[filename], f, indent=4)


def main_task(in_folder, index_file, out_folder):
    index = get_index(index_file)
    docs = get_files(in_folder)
    docs = add_meta_data(docs, index)
    save_docs(docs, out_folder)
    LOGGER.info("The operation completed successfully")


def config_logging():
    with open("logging.yml") as f:
        conf = yaml.safe_load(f)
    conf["loggers"]["com.mycompnay.app"]["level"] = os.getenv("LOGGING_LEVEL", "INFO")
    logging.config.dictConfig(conf)


# aws --profile=beeri --region=il-central-1 s3 ls ifat.beeri.org.il/ > local/all-objects.txt
# grep -v PRE all-objects.txt > files.txt
def parse_arguments():
    epilog = "Example: ifat.create-docs.py --in-folder local/files --index-file ifat-index.csv --out-folder local/docs"
    parser = argparse.ArgumentParser(description='This script imports files to archie with metadata from ifat index file', epilog=epilog)
    parser.add_argument('--in-folder', '-f', type=str, required=True, help='folder with files downloaded from the internet')
    parser.add_argument('--index-file', '-i', type=str, required=True, help='ifat index file')
    parser.add_argument('--out-folder', '-o', type=str, required=True, help='output folder')
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_arguments()
    config_logging()
    main_task(in_folder=args.in_folder, index_file=args.index_file, out_folder=args.out_folder)
