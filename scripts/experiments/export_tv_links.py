#!/usr/bin/env python3

import argparse
import csv


BUCKET = "https://s3.il-central-1.amazonaws.com/ifat.beeri.org.il"


def parse_arguments():
    epilog = "Example: app.py --in input.csv --out local"
    parser = argparse.ArgumentParser(description='This script creates a list with tv media links.', epilog=epilog)
    parser.add_argument('--infile', '-i', type=str, required=True, help='CSV input file')
    # parser.add_argument('--outfile', '-o', type=str, required=True, help='CSV output file')
    args = parser.parse_args()
    return args


def main_task(infile):
    with open(infile, newline='') as f:
        # ItemID,Media,Link. Media = אינטרנט | טלויזיה | עיתונות | רדיו
        reader = csv.DictReader(f)
        for record in reader:
            if record["Media"] == "אינטרנט":
                bucket_link = "{}/{}.pdf".format(BUCKET, record["ItemID"])
                print(bucket_link)


if __name__ == "__main__":
    args = parse_arguments()
    main_task(infile=args.infile)
