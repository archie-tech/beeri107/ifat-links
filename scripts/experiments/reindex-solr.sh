#!/bin/bash

# delete
curl http://localhost:8983/solr/archie_beeri/update --data '<delete><query>*:*</query></delete>' -H 'Content-type:text/xml; charset=utf-8'
curl http://localhost:8983/solr/archie_beeri/update --data '<commit/>' -H 'Content-type:text/xml; charset=utf-8'

# ingest
cmd="curl http://localhost:8983/solr/archie_beeri/update/json/docs -X POST -H 'Content-type:application/json' -d"
find local/queue -name "*.json" -exec $cmd @{} \;