#!/bin/bash

# export AWS_PROFILE=beeri

if [ $# -lt 1 ]; then
	echo "usage: $0 local-folder"
	exit 1
fi

aws --region il-central-1 s3 sync $1 s3://ifat.beeri.org.il/