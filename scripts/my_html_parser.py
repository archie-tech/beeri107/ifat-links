from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):

    url = ""
    ext = ""

    def set_ext(self, ext):
        self.ext = ext

    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for attr in attrs:
                if attr[0] == "href" and attr[1].endswith(self.ext):
                    self.url = attr[1]
