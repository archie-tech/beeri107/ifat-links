#!/bin/bash

# export AWS_PROFILE=beeri

if [ $# -lt 1 ]; then
	echo "usage: $0 in_file"
	exit 1
fi

while read -r line; do
    aws --region il-central-1 s3 rm "s3://ifat.beeri.org.il/${line}.pdf"
done < $1
