#!/bin/bash

if [ $# -lt 2 ]; then
	echo "usage: $0 in_file out_folder"
	exit 1
fi

function download_with_httrack {
	echo "processing file $IN_FILE"
	local options=" \
	--extended-parsing=0 \
	--structure=4 \
	--build-top-index=0 \
	--index=0 \
	--near \
	--extra-log \
	--debug-log \
	--retries=1 \
	--robots=0 \
	--disable-security-limits \
	--keep-alive \
	--max-rate=100000000000000000 \
	--connection-per-second=50 \
	--urlhack \
	--sockets=80 \
	--advanced-maxlinks=500000000 \
	--check-type=2 \
	--mirrorlinks \
	--depth=1 \
	--user-agent='Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/999.99 (KHTML, like Gecko) Chrome/70.0.2743.98 Mobile' \
	"

	while read -r line; do
		id=$(echo $line | cut -d , -f 1)
		url=$(echo $line | cut -d , -f 2)
		echo "downloading $url to $id"
		httrack $url $options -O ${OUT_FOLDER}/${id}
	done <$IN_FILE
}

function download_with_wget {
	local options="--mirror --convert-links --adjust-extension --page-requisites --no-parent"
	while read -r line; do
		id=$(echo $line | cut -d , -f 1)
		url=$(echo $line | cut -d , -f 2)
		echo "downloading $url to $id"
		mkdir ${OUT_FOLDER}/${id}
		#cd $id && wget $WGET_OPTION $url && cd ..
		wget $options --directory-prefix=${OUT_FOLDER}/${id} $url
	done <$IN_FILE
}

function download_with_wkhtmltopdf {
	while read -r line; do
		id=$(echo $line | cut -d , -f 1)
		url=$(echo $line | cut -d , -f 2)
		echo "downloading $url to $id"
		wkhtmltopdf "$url" ${OUT_FOLDER}/${id}.pdf
	done <$IN_FILE

}

IN_FILE=$1
OUT_FOLDER=$2

download_with_httrack
#download_with_wget
#download_with_wkhtmltopdf
