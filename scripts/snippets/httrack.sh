#!/bin/bash

if [ $# -lt 1 ]; then
	echo "usage: $0 url"
	exit 1
fi

# httrack "$1" www.ifat.com -* +mime:text/html +mime:video/mp4 -near --depth=2
httrack "$1" www.ifat.com
# --near --depth=2
# -* +mime:text/html +mime:image/*