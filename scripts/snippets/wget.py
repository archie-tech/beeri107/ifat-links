import logging
import os
import subprocess

def download_with_wget(folder, url):
    cmd = [
        "wget",
        "--quiet",
        "--directory-prefix=" + folder,
        "--mirror",
        "--convert-links",
        "--adjust-extension",
        "--page-requisites",
        "--no-parent",
        url
    ]
    logging.info(cmd)
    try:
        os.mkdir(folder)
        subprocess.check_call(cmd)
    except subprocess.CalledProcessError as e:
        logging.warning(e)
