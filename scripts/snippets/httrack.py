import logging
import os
import subprocess

def download_with_httrack(folder, url):
    cmd = [
        "httrack",
        url,
        "-O", folder,
        "--mirrorlinks",
        "--depth=1",
        "--near",
        # "--extended-parsing=0",
        # "--structure=4",
        # "--build-top-index=0",
        # "--index=0",
        # "--extra-log",
        # "--debug-log",
        # "--retries=1",
        # "--robots=0",
        # "--disable-security-limits",
        # "--keep-alive",
        # "--max-rate=100000000000000000",
        # "--connection-per-second=50",
        # "--urlhack",
        # "--sockets=80",
        # "--advanced-maxlinks=500000000",
        # "--check-type=2",
        # "--user-agent='Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/999.99 (KHTML, like Gecko) Chrome/70.0.2743.98 Mobile'",
    ]
    logging.info(cmd)
    try:
        os.mkdir(folder)
        subprocess.check_call(cmd)
    except subprocess.CalledProcessError as e:
        logging.warning(e)
