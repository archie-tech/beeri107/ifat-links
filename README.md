# Beeri October 7 - Iifat links

This project contains scripts and other resources for downloading media and html files from Ifat service and save it on AWS S3 bucket. See [requirements](https://docs.google.com/document/d/1x2WfMZYUzsPYccVeRtXiKx4dugyHEr8tm_5KduYJy4w) for more details.

* Input: a csv file, each record contains 3 fields: 
  * item id - unique identifier
  * media - could be used to decide how to download the resource
  * link - where to download the resource from
* Output (download stage): for each record, a file on the local work-station (name = item id).

## Tools

Download

* [HTTrack](https://www.httrack.com)
* [Wget](https://www.gnu.org/software/wget)
* [wkhtmltopdf](https://wkhtmltopdf.org/)

Upload

* [AWS CLI](https://aws.amazon.com/cli)

## Download Logic

![download logic diagram](docs/download-logic.png)

## Solr setup

cd /opt/apache/solr
cp -R ~/Projects/archie/infrastructure/resources/solr_server/codedeploy/setup/ .
./setup/scripts/configure_solr.sh


## Solr Indexing

curl -X POST -H 'Content-Type: application/json' 'http://localhost:8983/solr/archie_beeri/update/json/docs' -d @doc1.json

https://solr.apache.org/guide/solr/9_2/indexing-guide/indexing-with-update-handlers.html

## Notes

Chrome -> Save page as -> Web Page, Complete (file name = ItemId)
